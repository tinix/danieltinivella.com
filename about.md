---
title: About
layout: page
permalink: "/about/"
---

Also known as Tinix , Software Engineer. Interested in low level programming and security, although my programming experience has been mostly related with Ruby

{% include image.html
img='images/tinix.png'
title="Me!"
caption="Hey, I am Tinix! " %}



I love working with Ruby and  Free/Open Source technologies and being able to contribute to them. Always involved in several projects some personal..., 
I enjoy trying new technologies and learning something new everyday. 
I like to spend my free time with friends, and I like drinking Mate, and 4 languages, but all that means almost nothing…
however I enjoy every code line!